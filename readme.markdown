Sudoku Solver
=============

## How to run

./solve input.txt

Where input.txt specifies a sudoku puzzle.
An example is provided in the repository.

